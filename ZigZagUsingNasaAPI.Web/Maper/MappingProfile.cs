﻿using AutoMapper;
using System;
using ZigZagUsingNasaAPI.Data.Entities;
using ZigZagUsingNasaAPI.Services.DTOs;
using ZigZagUsingNasaAPI.Web.Models;

namespace ZigZagUsingNasaAPI.Web.Maper
{
    public class MappingProfile : Profile
    {
        public MappingProfile()
        {
            CreateMap<NasaIMG, ImgMapper>().ForMember(dest => dest.Date, opt => opt.MapFrom(src => src.Date)).ReverseMap();
            

                 CreateMap<NasaImageViewModel, ImgMapper>()
                .ForMember(dest => dest.Date, opt => opt.MapFrom(src => DateTime.Parse(src.Date))).ReverseMap();
        }
    }
}
