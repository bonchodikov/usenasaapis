﻿using System.ComponentModel.DataAnnotations;

namespace ZigZagUsingNasaAPI.Web.Models
{
    public class NasaImageViewModel
    {
        [Required]        
        public string Date { get; set; }
        public string Explanation { get; set; }
        public string Title { get; set; }
        public string Url { get; set; }
    }
}
