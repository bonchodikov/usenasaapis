﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Net.Http;
using System.Threading.Tasks;
using AutoMapper;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using ZigZagUsingNasaAPI.Web.Models;
using ZigZagUsingNasaAPI.Data.Entities;
using ZigZagUsingNasaAPI.Services.Contracts;
using ZigZagUsingNasaAPI.Services.DTOs;

namespace ZigZagUsingNasaAPI.Web.Controllers
{
    public class HomeController : Controller
    {
        private readonly INasaDataService _service;
        private readonly IDataImageCRUD _imageService;

        private readonly IMapper _mapper;

        public HomeController(INasaDataService service, IMapper mapper, IDataImageCRUD imageService)
        {
            _service = service;
            _imageService = imageService;
            _mapper = mapper;
        }

        public async Task<IActionResult> Index()
        {
            var currentImg = await this._service.GetImage();
            var vm = _mapper.Map<NasaImageViewModel>(currentImg);
            return View(vm);
        }

        [Authorize]
        public async Task<IActionResult> Change(ImgMapper model)
        {
            DateTime temp;
            if (ModelState.IsValid && DateTime.TryParse(model.Date, out temp))
            {
                if (this._imageService.IsImageWithDateExists(DateTime.Parse(model.Date)))
                {
                      return View("Index", this._imageService.GetImageByDate(DateTime.Parse(model.Date), this._mapper));
                }

                var currentImg = await this._service.GetImage(model.Date);
                var vm = _mapper.Map<NasaImageViewModel>(currentImg);

                if(vm is null)
                {
                    //TODO: middleware/ error page for null values. 
                   return RedirectToAction("Index");
                   // return View(new ErrorViewModel { RequestId = Activity.Current?.Id ?? HttpContext.TraceIdentifier });
                }

                return View("Index", vm);
            }
            return View("Index", _mapper.Map<NasaImageViewModel>(model));
        }

        [Authorize]
        public async Task<IActionResult> Save()
        {
            if (ModelState.IsValid)
            {
                var currentImg = await this._service.GetImage();
                var result = this._imageService.CreateImage(currentImg, this._mapper);
                return View("Index", this._mapper.Map<NasaImageViewModel>(result));
            }
            return View("Index");
        }
        
        public async Task<IActionResult> ExcellExporter()
        {
            var response = this._service.GetResponce();
            var ms = await this._service.GetFile(response);
            ms.Seek(0, 0);
           
            return File(ms, "application/vnd.ms-excel", "ReportNEO.xls");          
        }
        public IActionResult Privacy()
        {
            return View();
        }

        [ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
        public IActionResult Error()
        {
            return View(new ErrorViewModel { RequestId = Activity.Current?.Id ?? HttpContext.TraceIdentifier });
        }
    }
}
