﻿using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore;
using ZigZagUsingNasaAPI.Data.Entities;

namespace ZigZagUsingNasaAPI.Data
{
    public class ZigZagUsingNasaAPIContext : IdentityDbContext<User, Role, int>
    {
        public ZigZagUsingNasaAPIContext(DbContextOptions<ZigZagUsingNasaAPIContext> options)
            : base(options)
        {
        }

        public DbSet<User> NasaUsers { get; set; }

        public DbSet<NasaIMG> Images { get; set; }

        //protected override void OnModelCreating(ModelBuilder model)
        //{
        //}

    }
}
