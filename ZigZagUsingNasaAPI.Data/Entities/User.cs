﻿using Microsoft.AspNetCore.Identity;

namespace ZigZagUsingNasaAPI.Data.Entities
{
    public class User : IdentityUser<int>
    {
        public string IpAddtrss { get; set; }
    }
}
    