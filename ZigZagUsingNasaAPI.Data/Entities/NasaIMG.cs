﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ZigZagUsingNasaAPI.Data.Entities
{
   public class NasaIMG
    {
        public Guid Id { get; set; }

        public string Copyright { get; set; }

        public DateTime Date { get; set; }

        public string Hdurl { get; set; }

        public string Explanation { get; set; }

        public string MediaType { get; set; }

        public string ServiceVersion { get; set; }

        public string Title { get; set; }

        public string Url { get; set; }
    }
}
