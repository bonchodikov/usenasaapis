﻿using AutoMapper;
using System;
using System.Linq;
using ZigZagUsingNasaAPI.Data;
using ZigZagUsingNasaAPI.Data.Entities;
using ZigZagUsingNasaAPI.Services.Contracts;
using ZigZagUsingNasaAPI.Services.DTOs;

namespace ZigZagUsingNasaAPI.Services.Services
{
    public class DataImageCRUD : IDataImageCRUD
    {
        private ZigZagUsingNasaAPIContext database;
        public DataImageCRUD(ZigZagUsingNasaAPIContext database)
        {
            this.database = database;
        }

        public ImgMapper CreateImage(ImgMapper imageToCreate, IMapper mapper)
        {
            if (!this.IsImageWithDateExists(DateTime.Parse(imageToCreate.Date)))
            {
                var imageToAdd = mapper.Map<NasaIMG>(imageToCreate);
                imageToAdd.Id = Guid.NewGuid();
                imageToAdd.Date = DateTime.Parse(imageToCreate.Date);
                this.database.Images.Add(imageToAdd);
                database.SaveChanges();
                return mapper.Map<ImgMapper>(imageToAdd);
            }
            else
            {
                return this.GetImageByDate(DateTime.Parse(imageToCreate.Date), mapper);
            }
        }

        public ImgMapper GetImageByDate(DateTime date, IMapper mapper)
        {
            var result = this.database.Images
                .FirstOrDefault(i=>i.Date.Year == date.Year && i.Date.Month == date.Month && i.Date.Day == date.Day);
                       
            return mapper.Map<ImgMapper>(result);
        }

        public bool IsImageWithDateExists(DateTime date)
        {
            return  this.database.Images
               .Any(i => i.Date.Year == date.Year && i.Date.Month == date.Month && i.Date.Day == date.Day);
        }

        public bool RemoveByDate(DateTime date)
        {
            throw new NotImplementedException();
        }

        public ImgMapper UpdateReport(ImgMapper image, IMapper mapper)
        {
            throw new NotImplementedException();
        }
    }
}
