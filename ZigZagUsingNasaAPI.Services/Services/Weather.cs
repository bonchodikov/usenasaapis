﻿using System.Net.Http;
using System.Net.Http.Headers;
using System.Web;

using ZigZagUsingNasaAPI.Services.Contracts;

namespace ZigZagUsingNasaAPI.Services.Services
{
    public class Weather : IWeather
    {
        private readonly IHttpClientFactory _clientFactory;
        public Weather(IHttpClientFactory clientFactory)
        {
            this._clientFactory = clientFactory;
        }
        private string GetIp()
        {
            var ip = System.Net.Dns.GetHostEntry(System.Net.Dns.GetHostName()).ToString();

            var x = System.Net.Dns.GetHostEntry(System.Net.Dns.GetHostName()).AddressList.GetValue(0).ToString();
            
            return ip;
        }
        public HttpResponseMessage GetResponce()
        {
            var httpClient = _clientFactory.CreateClient("HttpClient");

            httpClient.DefaultRequestHeaders.Accept.
                Add(new MediaTypeWithQualityHeaderValue("application/json"));

            var response = httpClient.GetAsync($"https://ip-api.com/" + this.GetIp()).GetAwaiter().GetResult();  // Blocking call! Program will wait here until a response is received or a timeout occurs.
            httpClient.Dispose();
            return response;
        }

    }
}
