﻿using System;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Threading.Tasks;
using ZigZagUsingNasaAPI.Services.Contracts;
using Newtonsoft.Json;
using ZigZagUsingNasaAPI.Services.DTOs;
using AutoMapper;
using Aspose.Cells;
using System.IO;
using Aspose.Cells.Utility;

namespace ZigZagUsingNasaAPI.Services.Services
{
    public class NasaDataService : INasaDataService
    {        
        private readonly IHttpClientFactory _clientFactory;
        private readonly IMapper _mapper;
        private readonly string key = "PEokybOTmLjUNtOYZMmBpCZsxETVnGo6eZiurnZ6";
       
        public NasaDataService(IHttpClientFactory clientFactory, IMapper mapper)
        {
            _clientFactory = clientFactory;
            _mapper = mapper;
        }

        public HttpResponseMessage GetResponce()
        {
            var httpClient = _clientFactory.CreateClient("HttpClient");           

            httpClient.DefaultRequestHeaders.Accept.
                Add(new MediaTypeWithQualityHeaderValue("application/json"));

            var response =  httpClient.GetAsync($"https://api.nasa.gov/neo/rest/v1/feed?start_date=2015-09-07&end_date=2015-09-08&api_key="+this.key).GetAwaiter().GetResult();  // Blocking call! Program will wait here until a response is received or a timeout occurs.
            httpClient.Dispose();
            return response;           
        }

        public async Task<ImgMapper> GetData()
        {
            var response = this.GetResponce();
            if (!response.IsSuccessStatusCode)
            {
                throw new ArgumentException();
            }
            else
            {
                var responseContent = await response.Content.ReadAsStringAsync();
                var mappedResult = JsonConvert.DeserializeObject<ImgMapper>(responseContent);
                return mappedResult;
            }
        }

        public async Task<ImgMapper> GetImage()
        {

            var httpClient = _clientFactory.CreateClient("HttpClient");

            httpClient.DefaultRequestHeaders.Accept.
                Add(new MediaTypeWithQualityHeaderValue("application/json"));

            var response = httpClient.GetAsync($"https://api.nasa.gov/planetary/apod?api_key="+this.key).GetAwaiter().GetResult();
            httpClient.Dispose();
            if (!response.IsSuccessStatusCode)
            {
                return null;

            }
            else
            {
                var responseContent = await response.Content.ReadAsStringAsync();
                var mappedResult = JsonConvert.DeserializeObject<ImgMapper>(responseContent);
                return mappedResult;
            }
        }


        public async Task<ImgMapper> GetImage(string date)
        {
            
            var httpClient = _clientFactory.CreateClient("HttpClient");

            httpClient.DefaultRequestHeaders.Accept.
                Add(new MediaTypeWithQualityHeaderValue("application/json"));

            var response = httpClient.GetAsync($"https://api.nasa.gov/planetary/apod?api_key="+this.key +"&date="+date).GetAwaiter().GetResult();
            httpClient.Dispose();
            if (!response.IsSuccessStatusCode)
            {
                return null;

            }
            else
            {
                var responseContent =  await response.Content.ReadAsStringAsync();
                var mappedResult = JsonConvert.DeserializeObject<ImgMapper>(responseContent);
                return mappedResult;
            }
        }      

        public async Task<MemoryStream> GetFile(HttpResponseMessage response)
        {
            string jsonInput = await response.Content.ReadAsStringAsync();
            Workbook workbook = new Workbook();
            Worksheet worksheet = workbook.Worksheets[0];
           
            // Set Styles
            CellsFactory factory = new CellsFactory();            
            Style style = factory.CreateStyle();
            
            style.HorizontalAlignment = TextAlignmentType.Center;
            style.Font.Color = System.Drawing.Color.Green;
            style.Font.IsBold = true;         


            // Set JsonLayoutOptions
            JsonLayoutOptions options = new JsonLayoutOptions();
            options.TitleStyle = style;
            options.ArrayAsTable = true;
            

            // Import JSON Data
            JsonUtility.ImportData(jsonInput, worksheet.Cells, 0, 0, options);

            // Save Excel file
            //workbook.Save("Import-Data-JSON-To-Excel.xlsx");
            var file = workbook.SaveToStream();

            return file;
        }
        
    }
}
