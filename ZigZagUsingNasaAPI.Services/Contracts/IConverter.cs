﻿using System.IO;
using System.Net.Http;
using System.Threading.Tasks;

namespace ZigZagUsingNasaAPI.Services.Contracts
{
  public  interface IConverter
    {
        Task<MemoryStream> GetFile(HttpResponseMessage response);
        HttpResponseMessage GetResponce();
    }
}
