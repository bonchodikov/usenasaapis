﻿using AutoMapper;
using System;
using ZigZagUsingNasaAPI.Data.Entities;
using ZigZagUsingNasaAPI.Services.DTOs;

namespace ZigZagUsingNasaAPI.Services.Contracts
{
    public interface IDataImageCRUD
    {
        bool IsImageWithDateExists(DateTime date);

        ImgMapper CreateImage(ImgMapper imageToCreate, IMapper mapper);

        ImgMapper GetImageByDate(DateTime date, IMapper mapper);

        ImgMapper UpdateReport(ImgMapper image, IMapper mapper);

        bool RemoveByDate(DateTime date);
    }
}
