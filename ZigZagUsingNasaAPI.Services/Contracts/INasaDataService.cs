﻿using System.Threading.Tasks;
using ZigZagUsingNasaAPI.Services.DTOs;

namespace ZigZagUsingNasaAPI.Services.Contracts
{
    public  interface INasaDataService:IConverter
    {
         Task<ImgMapper> GetData();

        Task<ImgMapper> GetImage();

        Task<ImgMapper> GetImage(string date);

    }
}
